<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
/**
 * This is the model class for table "eventos".
 *
 * @property int $idEvento
 * @property string $Nombre
 * @property string $Direccion
 * @property string $Descripcion
 * @property int|null $IdUsuario
 * @property int $IdTipo
 * @property int $IdArtista
 * @property string $Fecha_Inicio
 * @property string $Fecha_Fin
 * * @property int $IdEntrada
 * @property int $CantidadEntradas
 * @property float $PrecioEntrada

 * @property Artistas $idArtista
 * @property Tiposeventos $idTipo
 * @property Usuarios $idUsuario
 */
class Eventos extends \yii\db\ActiveRecord 
{
    public $imagefile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Direccion', 'Descripcion', 'IdTipo', 'IdArtista', 'Fecha_Inicio', 'Hora_Inicio',  'Fecha_Fin',  'Hora_Fin', 'CantidadEntradas', 'PrecioEntrada'], 'required'],
            [['IdUsuario', 'idEvento', 'IdTipo', 'IdArtista','IdEntrada', 'VentaAnticipada', 'CantidadEntradas'], 'integer'],
            [['CantidadEntradas'],'compare', 'compareValue' => 1000, 'operator' => '<='],
            [['Fecha_Inicio', 'Hora_Inicio', 'IdEntrada', 'Fecha_Fin', 'Hora_Fin', 'VentaAnticipada', 'Fecha_VentaAnticipada', 'Hora_VentaAnticipada', 'Foto'], 'safe'],
            ['Fecha_Inicio', 'validateDates'],
            [['PrecioEntrada'],'compare', 'compareValue' => 30, 'operator' => '<='],
            [['Nombre', 'Direccion'], 'string', 'max' => 45],
            [['Descripcion'], 'string', 'min'=>10, 'max'=>450],
           // [['imagefile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['IdArtista'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::className(), 'targetAttribute' => ['IdArtista' => 'IdArtista']],
            [['IdTipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposeventos::className(), 'targetAttribute' => ['IdTipo' => 'IdTipo']],
            [['IdUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['IdUsuario' => 'IdUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEvento' => 'Id Evento',
            'Nombre' => 'Nombre',
            'Direccion' => 'Direccion',
            'Descripcion' => 'Descripcion',
            'IdUsuario' => 'Id Usuario',
            'IdTipo' => 'Id Tipo',
            'IdArtista' => 'Id Artista',
            'Fecha_Inicio' => 'Fecha Inicio',
            'Hora_Inicio' => 'Hora Inicio',
            'Fecha_Fin' => 'Fecha Fin',
            'Hora_Fin' => 'Hora Fin',
            'IdEntrada' => 'Id Entrada',
            'CantidadEntradas' => 'Cantidad Entradas',
            'PrecioEntrada' => 'Precio Entrada',
            'Foto'=>'Foto',
            'VentaAnticipada'=>'Venta Anticipada',
            'Fecha_VentaAnticipada' => 'Fecha Venta Anticipada',
            'Hora_VentaAnticipada' => 'Hora Venta Anticipada',
        ];
    }
    public function beforeSave($insert) {
        if($this->isNewRecord)
            $this->idEvento=Null;$this->IdUsuario=Yii::$app->user->identity->IdUsuario;$this->IdEntrada=$this->idEvento;
        return parent::beforeSave($insert);
    }
 /**
     * Gets query for [[Compraentradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompraentradas()
    {
        return $this->hasMany(Compraentradas::className(), ['idEvento' => 'idEvento']);
    }
    /**
     * Gets query for [[IdArtista]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArtista()
    {
        return $this->hasOne(Artistas::className(), ['IdArtista' => 'IdArtista']);
    }

    /**
     * Gets query for [[IdTipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tiposeventos::className(), ['IdTipo' => 'IdTipo']);
    }
    
    
    /** 
     * Gets query for [[IdUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['IdUsuario' => 'IdUsuario']);
    }
    public static function getIdTipoText($condition=''){
        return ArrayHelper::map(self::find()->where($condition)->all(),'IdTipo', 'Descrip');
      }
      public function getfechaIni(){
        return \Yii::$app->formatter->asDate($this->Fecha_Inicio);
    }
    public function gethoraIni(){
        return \Yii::$app->formatter->asTime($this->Hora_Inicio, 'medium');
    }
    public function getfechaF(){
        return \Yii::$app->formatter->asDate($this->Fecha_Fin);
    }
    public function gethoraF(){
        return \Yii::$app->formatter->asTime($this->Hora_Fin, 'medium');
    }
    public function getfechaAnt(){
        return \Yii::$app->formatter->asDate($this->Fecha_VentaAnticipada);
    }
    public function gethoraAnt(){
        return \Yii::$app->formatter->asTime($this->Hora_VentaAnticipada, 'medium');
    }
    public function validateDates(){
        if(strtotime($this->Fecha_Fin) <= strtotime($this->Fecha_Inicio)){
            $this->addError('Fecha_Inicio','La fecha de inicio no puede ser superior a la fecha final');
            $this->addError('Fecha_Fin','La fecha final no puede ser inferior a la fecha de inicio');
        }
    }
    
    public function upload()
    {
        
        if ($this->validate()) {
            $this->Foto=$this->idEvento.'_'. substr(md5(time()),0,4).'.'.$this->imagefile->extension;
            $this->imagefile->saveAs('imagenes/'. $this->Foto);
            return true;
        } else {
            return true;
        }
    }
   
    
}
