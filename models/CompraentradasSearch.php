<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Compraentradas;

/**
 * CompraentradasSearch represents the model behind the search form of `app\models\Compraentradas`.
 */
class CompraentradasSearch extends Compraentradas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idEvento', 'idUsuario', 'CantidadEntradas', 'Puntos'], 'integer'],
            [['Fecha_Compra'], 'safe'],
            [['Importe'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Compraentradas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idEvento' => $this->idEvento,
            'idUsuario' => $this->idUsuario,
            'CantidadEntradas' => $this->CantidadEntradas,
            'Puntos' => $this->Puntos,
            'Fecha_Compra' => $this->Fecha_Compra,
            'Importe' => $this->Importe,
        ]);

        return $dataProvider;
    }
}
