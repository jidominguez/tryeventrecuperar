<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compraentradas".
 *
 * @property int $id
 * @property int $idEvento
 * @property int $idUsuario
 * @property int $Puntos
 * @property string $Fecha_Compra
 * @property float $Importe
 *
 * @property Eventos $idEvento0
 * @property Usuarios $idUsuario0
 */
class Compraentradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compraentradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvento', 'idUsuario', 'CantidadEntradas', 'Puntos', 'Fecha_Compra', 'Importe'], 'required'],
            [['idEvento', 'idUsuario', 'CantidadEntradas', 'Puntos'], 'integer'],
            [['Fecha_Compra'], 'safe'],
            [['Importe'], 'number'],
            [['idEvento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['idEvento' => 'idEvento']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['idUsuario' => 'IdUsuario']],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEvento' => 'Id Evento',
            'idUsuario' => 'Id Usuario',
            'CantidadEntradas' => 'Cantidad Entradas',
            'Puntos' => 'Puntos',
            'Fecha_Compra' => 'Fecha Compra',
            'Importe' => 'Importe',
        ];
    }
 
    /**
     * Gets query for [[IdEvento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasOne(Eventos::className(), ['idEvento' => 'idEvento']);
    }

    /**
     * Gets query for [[IdUsuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['IdUsuario' => 'idUsuario']);
    }
    
}
