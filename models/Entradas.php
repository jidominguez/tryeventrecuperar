<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $idEntrada
 * @property string $Fecha_inicio
 * @property string $Fecha_final
 * @property int $Precio
 * @property int $Disponible
 * @property int $IdCliente
 * @property int $IdEvento
 *
 * @property Usuarios $idCliente
 * @property Eventos $idEvento
 * @property HistorialComprasEntradasCab[] $historialComprasEntradasCabs
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEntrada', 'Fecha_inicio', 'Fecha_final', 'Precio', 'Disponible', 'IdCliente', 'IdEvento'], 'required'],
            [['idEntrada', 'Precio', 'Disponible', 'IdCliente', 'IdEvento'], 'integer'],
            [['Fecha_inicio', 'Fecha_final'], 'safe'],
            [['idEntrada'], 'unique'],
            [['IdCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['IdCliente' => 'IdUsuario']],
            [['IdEvento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['IdEvento' => 'idEvento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEntrada' => 'Id Entrada',
            'Fecha_inicio' => 'Fecha Inicio',
            'Fecha_final' => 'Fecha Final',
            'Precio' => 'Precio',
            'Disponible' => 'Disponible',
            'IdCliente' => 'Id Cliente',
            'IdEvento' => 'Id Evento',
        ];
    }

    /**
     * Gets query for [[IdCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente()
    {
        return $this->hasOne(Usuarios::className(), ['IdUsuario' => 'IdCliente']);
    }

    /**
     * Gets query for [[IdEvento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEvento()
    {
        return $this->hasOne(Eventos::className(), ['idEvento' => 'IdEvento']);
    }

    /**
     * Gets query for [[HistorialComprasEntradasCabs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistorialComprasEntradasCabs()
    {
        return $this->hasMany(HistorialComprasEntradasCab::className(), ['IdEntrada' => 'idEntrada']);
    }
}
