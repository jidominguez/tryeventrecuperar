<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Eventos;

/**
 * EventosSearch represents the model behind the search form of `app\models\Eventos`.
 */
class EventosSearch extends Eventos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvento', 'IdUsuario', 'IdTipo', 'IdArtista', 'IdEntrada', 'CantidadEntradas'], 'integer'],
            [['Nombre', 'Direccion', 'Descripcion', 'Fecha_Inicio', 'Fecha_Fin'], 'safe'],
            [['PrecioEntrada'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Eventos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idEvento' => $this->idEvento,
            'IdUsuario' => $this->IdUsuario,
            'IdTipo' => $this->IdTipo,
            'IdArtista' => $this->IdArtista,
            'Fecha_Inicio' => $this->Fecha_Inicio,
            'Fecha_Fin' => $this->Fecha_Fin,
            'IdEntrada' => $this->IdEntrada,
            'CantidadEntradas' => $this->CantidadEntradas,
            'PrecioEntrada' => $this->PrecioEntrada,
            'VentaAnticipada' => $this->VentaAnticipada
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'Direccion', $this->Direccion])
            ->andFilterWhere(['like', 'Descripcion', $this->Descripcion]);

        return $dataProvider;
    }
}
