<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artistas".
 *
 * @property int $IdArtista
 * @property string $Nombre
 * @property string|null $Descrip
 *
 * @property Eventos[] $eventos
 */
class Artistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdArtista', 'Nombre'], 'required'],
            [['IdArtista'], 'integer'],
            [['Nombre', 'Descrip'], 'string', 'max' => 45],
            [['IdArtista'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdArtista' => 'Id Artista',
            'Nombre' => 'Nombre',
            'Descrip' => 'Descrip',
        ];
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['IdArtista' => 'IdArtista']);
    }
}
