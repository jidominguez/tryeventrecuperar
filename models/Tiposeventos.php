<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposeventos".
 *
 * @property int $IdTipo
 * @property string $Descrip
 *
 * @property Eventos[] $eventos
 */
class Tiposeventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiposeventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdTipo', 'Descrip'], 'required'],
            [['IdTipo'], 'integer'],
            [['Descrip'], 'string', 'max' => 45],
            [['IdTipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdTipo' => 'Id Tipo',
            'Descrip' => 'Descrip',
        ];
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['IdTipo' => 'IdTipo']);
    }
}
