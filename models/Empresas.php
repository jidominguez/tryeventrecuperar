<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas".
 *
 * @property int $IdEmpresa
 * @property string $Nombre
 * @property string $Usuario
 * @property string $Email
 * @property int $Baneado
 *
 * @property Eventos[] $eventos
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Usuario', 'Email'], 'required'],
            [['Baneado'], 'integer'],
            [['Nombre', 'Usuario', 'Email'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdEmpresa' => 'Id Empresa',
            'Nombre' => 'Nombre',
            'Usuario' => 'Usuario',
            'Email' => 'Email',
            'Baneado' => 'Baneado',
        ];
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['IdEmpresa' => 'IdEmpresa']);
    }
}
