<?php

namespace app\models;

use Yii;
use app\models\Entradas;
use app\models\RolesDeUsuario;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $IdUsuario
 * @property string $Nombre
 * @property string $Usuario
 * @property int $Punto
 * @property string $Contrasenya
 * @property string $Email
 * @property string $Fecha_Nacimiento
 * @property int $Baneado
 * @property int $IdRol
 *
 * @property Entradas[] $entradas
 * @property HistorialComprasProductosCab[] $historialComprasProductosCabs
 * @property RolesDeUsuario $idRol
 */
class Usuarios extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 2;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }
    
    public static $RolOptions=['1'=>'Administrador', '2'=>'Comprador', '3'=>'Publicador'];
    public function __toString(){
        return $this->nombre;
    }
    /**
     * {@inheritdoc}
     */
    
    public function rules()
    {
        return [
            [['Nombre', 'Usuario', 'Contrasenya', 'Email', 'Fecha_Nacimiento', 'IdRol'], 'required', 'message'=>' (▀̿̿Ĺ̯̿▀̿ ̿) Necesitamos esa información  (▀̿̿Ĺ̯̿▀̿ ̿)'],
            [['IdUsuario', 'Punto', 'Baneado', 'IdRol'], 'integer'],
            [['Fecha_Nacimiento'], 'safe'],
            ['Fecha_Nacimiento', 'compareDates'],
            [['Nombre', 'Usuario', 'Email'], 'string', 'max' => 45],
            [['Contrasenya'], 'string', 'min'=>8, 'max' => 45],
            [['IdUsuario'], 'unique'],
            [['IdRol'], 'exist', 'skipOnError' => true, 'targetClass' => RolesDeUsuario::className(), 'targetAttribute' => ['IdRol' => 'IdRol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdUsuario' => 'Id Usuario',
            'Nombre' => 'Nombre',
            'Usuario' => 'Usuario',
            'Punto' => 'Punto',
            'Contrasenya' => 'Contrasenya',
            'Email' => 'Email',
            'Fecha_Nacimiento' => 'Fecha Nacimiento',
            'Baneado' => 'Baneado',
            'IdRol' => 'Id Rol',
        ];
    }

      /**
     * Gets query for [[Compraentradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompraentradas()
    {
        return $this->hasMany(Compraentradas::className(), ['idUsuario' => 'IdUsuario']);
    }
    /**
     * Gets query for [[IdRol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(RolesDeUsuario::className(), ['IdRol' => 'IdRol']);
    }
   //Esto es para registrar con los valores por defecto
   public function beforeSave($insert) {
    if($this->isNewRecord) $this->Punto=0; $this->IdUsuario=Null; $this->Baneado=0;
    if(strlen($this->Contrasenya)!=32) $this->Contrasenya=md5($this->Contrasenya);
    return parent::beforeSave($insert);
}
//Para el login usando los datos de la base de datos
public static function findByUsername($username) {
    return static::findOne(['Usuario' => $username]);
  }
 
  public static function findIdentity($id) {
     return static::findOne($id);
  }
 
  public function getId() {
      return $this->IdUsuario;
  }
 
  public function getAuthKey() { }
 
  public function validateAuthKey($authKey) { }
  public static function findIdentityByAccessToken($token, $type = null) {}  
 
  // Comprueba que el password que se le pasa es correcto
  public function validatePassword($password) {
       return $this->Contrasenya === md5($password); // Si se utiliza otra función de encriptación distinta a md5, habrá que cambiar esta línea
  }
  public function getfechaText(){
    return \Yii::$app->formatter->asDate($this->Fecha_Nacimiento);
}
public function getRolText(){
    if(!$this->IdRol){
        return '';
    }else{
        return self::$RolOptions[$this->IdRol]??'';
    }
}
public function compareDates(){
    if(strtotime(Yii::$app->formatter->asDate(date('Y-m-d'))) <= strtotime($this->Fecha_Nacimiento)){
        $this->addError('Fecha_Nacimiento','Es imposible que hayas nacido ese día');
    }
}


}