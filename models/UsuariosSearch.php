<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $IdUsuario
 * @property string $Nombre
 * @property string $Usuario
 * @property int $Punto
 * @property string $Contrasenya
 * @property string $Email
 * @property string $Fecha_Nacimiento
 * @property int $Baneado
 * @property int $IdRol
 *
 * @property Entradas[] $entradas
 * @property Eventos[] $eventos
 * @property RolesDeUsuario $idRol
 */
class UsuariosSearch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Usuario', 'Contrasenya', 'Email', 'Fecha_Nacimiento', 'IdRol'], 'required'],
            [['Punto', 'Baneado', 'IdRol'], 'integer'],
            [['Fecha_Nacimiento'], 'safe'],
            [['Nombre', 'Usuario', 'Contrasenya', 'Email'], 'string', 'max' => 45],
            [['IdRol'], 'exist', 'skipOnError' => true, 'targetClass' => RolesDeUsuario::className(), 'targetAttribute' => ['IdRol' => 'IdRol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdUsuario' => 'Id Usuario',
            'Nombre' => 'Nombre',
            'Usuario' => 'Usuario',
            'Punto' => 'Punto',
            'Contrasenya' => 'Contrasenya',
            'Email' => 'Email',
            'Fecha_Nacimiento' => 'Fecha Nacimiento',
            'Baneado' => 'Baneado',
            'IdRol' => 'Id Rol',
        ];
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['IdUsuario' => 'IdUsuario']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::className(), ['IdUsuario' => 'IdUsuario']);
    }

    /**
     * Gets query for [[IdRol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(RolesDeUsuario::className(), ['IdRol' => 'IdRol']);
    }
}
