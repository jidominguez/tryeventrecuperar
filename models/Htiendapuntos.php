<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "htiendapuntos".
 *
 * @property int $id
 * @property int $idEvento
 * @property int $IdUsuarioH
 *
 * @property Eventos $idEvento0
 */
class Htiendapuntos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'htiendapuntos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvento', 'IdUsuarioH'], 'required'],
            [['idEvento', 'IdUsuarioH'], 'integer'],
            [['idEvento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['idEvento' => 'idEvento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEvento' => 'Id Evento',
            'IdUsuarioH' => 'Id Usuario H',
        ];
    }

    /**
     * Gets query for [[IdEvento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasMany(Eventos::className(), ['idEvento' => 'idEvento']);
    }
}
