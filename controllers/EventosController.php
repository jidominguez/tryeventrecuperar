<?php

namespace app\controllers;

use app\models\Compraentradas;
use Yii;
use app\models\Eventos;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use app\models\EventosSearch;
use yii\web\NotFoundHttpException;

/**
 * EventosController implements the CRUD actions for Eventos model.
 */
class EventosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Eventos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventosSearch();
        $searchModel->VentaAnticipada=0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Eventos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Eventos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Eventos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->imagefile = UploadedFile::getInstance($model, 'imagefile');
            if ($model->imagefile && $model->upload()) {
                $model->save(); //para guardar Foto
            }
            return $this->redirect(['view', 'id' => $model->idEvento]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
   

    /**
     * Updates an existing Eventos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idEvento]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Eventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Eventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Eventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Eventos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

public function actionSearchadv()
{
    $model = new \app\models\EventosSearch();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // form inputs are valid, do something here
            return;
        }
    }

    return $this->render('searchadv', [
        'model' => $model,
    ]);
}
public function actionTiendapuntos()
{
    $searchModel = new EventosSearch();
    $searchModel->VentaAnticipada=1;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('tiendapuntos', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}


/*public function actionCheckout(){
    // Setup order information array with all items
    $params = [
        'method'=>'paypal',
        'intent'=>'sale',
        'order'=>[
            'description'=>'Payment description',
            'subtotal'=>44,
            'shippingCost'=>0,
            'total'=>44,
            'currency'=>'USD',
            'items'=>[
                [
                    'name'=>'Item one',
                    'price'=>10,
                    'quantity'=>1,
                    'currency'=>'USD'
                ],
                [
                    'name'=>'Item two',
                    'price'=>12,
                    'quantity'=>2,
                    'currency'=>'USD'
                ],
                [
                    'name'=>'Item three',
                    'price'=>1,
                    'quantity'=>10,
                    'currency'=>'USD'
                ],

            ]

        ]
    ];
    
    // In this action you will redirect to the PayPpal website to login with you buyer account and complete the payment
    Yii::$app->PayPalRestApi->checkOut($params);
}

public function actionMakePayment(){
     // Setup order information array 
    $params = [
        'order'=>[
            'description'=>'Payment description',
            'subtotal'=>44,
            'shippingCost'=>0,
            'total'=>44,
            'currency'=>'USD',
        ]
    ];
  // In case of payment success this will return the payment object that contains all information about the order
  // In case of failure it will return Null
  return  Yii::$app->PayPalRestApi->processPayment($params);

}*/
}
