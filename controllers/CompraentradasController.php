<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Usuarios;
use yii\filters\VerbFilter;
use app\models\Compraentradas;
use app\models\Eventos;
use yii\web\NotFoundHttpException;
use app\models\CompraentradasSearch;

/**
 * CompraentradasController implements the CRUD actions for Compraentradas model.
 */
class CompraentradasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Compraentradas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompraentradasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compraentradas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Compraentradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Compraentradas();
        $model->idUsuario=Yii::$app->user->identity->IdUsuario;
        $model->Fecha_Compra=date('Y-m-d');
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $connection = Yii::$app->db;
                $command = $connection->createCommand('UPDATE usuarios SET Punto=Punto+'.$model->Puntos.' WHERE IdUsuario='.Yii::$app->user->identity->IdUsuario);
                $restarentradas= $connection->createCommand('UPDATE eventos SET CantidadEntradas=CantidadEntradas-'.$model->CantidadEntradas.' WHERE idEvento='.$model->idEvento);
                $command->execute();
                $restarentradas->execute();
                return $this->redirect(['view', 'id' => $model->id]);
        }
//var_dump($model->getErrors());
//die();
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCompratiendapuntos()
    {
        $model = new Compraentradas();
        $model->idUsuario=Yii::$app->user->identity->IdUsuario;
        $model->Fecha_Compra=date('Y-m-d');
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $connection = Yii::$app->db;
            $command = $connection->createCommand('UPDATE usuarios SET Punto=Punto-150 WHERE IdUsuario='.Yii::$app->user->identity->IdUsuario);
            $command->execute();
            Yii::$app->mailer->compose()
                ->setFrom('from@domain.com')
                ->setTo($model->Email)
                ->setSubject('Message subject')
                ->setTextBody('Entradas')
                ->setHtmlBody('<b>HTML content</b>')
                ->send();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
 

    /**
     * Updates an existing Compraentradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Compraentradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
 

    /**
     * Finds the Compraentradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Compraentradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Compraentradas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
