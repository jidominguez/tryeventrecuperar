<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use scotthuangzl\googlechart\GoogleChart;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> <?php
    //$books = ArrayHelper::map(\app\models\Tiposeventos::find()->all(), 'IdTipo', 'Descrip');
    
    //$data = array( 
      //  array('Esto', 'Aquello'), 
        //array('Datos', Yii::$app->db->createCommand('SELECT Descrip, count(*) numeventos FROM eventos e, tiposeventos t WHERE IdTipo=6')->queryScalar()));
    //echo GoogleChart::widget(array('visualization'=>'PieChart', 'data'=>$data));
       ?>

    </p>
    <p>
    <?php
    /*$meses = array(1=>'Enero',2=>'Febrero',3=>'Marzo',4=>'Abril', 5=>'Mayo', 6=>'Junio', 7=>'Julio', 8=>'Agosto',9=>'Septiembre', 10=>'Octubre', 11=>'Noviembre', 12=>'Diciembre');
    
        foreach($meses as $numero=>$nombre)
	    {
            if(10>$numero){
                echo " El 0" . $numero . " es " . $nombre;
            }else{
                echo " El " . $numero . " es " . $nombre;
            }
	    } */
    /*echo GoogleChart::widget(array('visualization' => 'BarChart',
    'data' => array(
        array('Task', 'Meses'),
        array('Enero', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200101 AND 20200131')->queryAll())),
        array('Febrero', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200201 AND 20200229')->queryAll())),
        array('Marzo', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200301 AND 20200331')->queryAll())),
        array('Abril', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200401 AND 20200430')->queryAll())),
        array('Mayo', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200501 AND 20200531')->queryAll())),
        array('Junio', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200601 AND 20200630')->queryAll())),
        array('Julio', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200701 AND 20200731')->queryAll())),
        array('Agosto', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200801 AND 20200831')->queryAll())),
        array('Septiembre', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20200901 AND 20200930')->queryAll())),
        array('Octubre', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20201001 AND 20201031')->queryAll())),
        array('Noviembre', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20201101 AND 20201130')->queryAll())),
        array('Diciembre', count(Yii::$app->db->createCommand('SELECT * FROM compraentradas WHERE Fecha_Compra BETWEEN 20201201 AND 20201231')->queryAll())),
        
    ),
    
    'options' => array('title' => 'Compra de entradas en base al año actual')));*/

    
    ?>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}{pager}',
        'itemOptions' => ['class' => 'items'],
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('vista',['model'=>$model]);
        },
    ]) ?>


</div>
