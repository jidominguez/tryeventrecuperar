<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */

$this->title = $model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="eventos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Nombre], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Nombre], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idEvento',
            'Nombre',
            'Direccion',
            'Descripcion',
            ['attribute'=>'IdUsuario',
            'value'=>function($model,$attribute){
                return $model->usuario->Nombre;
            }
            ],
            'IdEntrada',
            ['attribute'=>'IdTipo',
            'value'=>function($model,$attribute){
                return $model->tipo->Descrip;
            }
            ],
            ['attribute'=>'IdArtista',
            'value'=>function($model,$attribute){
                return $model->artista->Nombre;
            }
            ],
            ['attribute'=>'Fecha_Inicio',
            'value'=>function($model,$attribute){
                return $model->fechaIni;  
            }
            ],
            ['attribute'=>'Hora_Inicio',
            'value'=>function($model,$attribute){
                return $model->HoraIni;  
            }
            ],
            ['attribute'=>'Fecha_Fin',
            'value'=>function($model,$attribute){
                return $model->fechaF;
            }
            ],
            ['attribute'=>'Hora_Fin',
            'value'=>function($model,$attribute){
                return $model->horaF;
            }
            ],            
            'CantidadEntradas',
            'PrecioEntrada',
            'VentaAnticipada',
            ['attribute'=>'Fecha_VentaAnticipada',
            'value'=>function($model,$attribute){
                return $model->fechaAnt;
            }
            ],
            ['attribute'=>'Hora_VentaAnticipada',
            'value'=>function($model,$attribute){
                return $model->horaAnt;
            }
            ],
        ]
    ]) ?>

</div>
