<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
/* @var $this yii\web\View */
/* @var $model app\models\EventosSearch */
/* @var $form ActiveForm */
$books = ArrayHelper::map(\app\models\Tiposeventos::find()->all(), 'IdTipo', 'Descrip');
?>
<div class="searchadv">
<div>
    <p><h1>Busqueda Avanzada</h1></p>
</div>
<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
?>
        <?= $form->field($model, 'Nombre') ?>
        <?= $form->field($model, 'Direccion') ?>
        <?= $form->field($model, 'IdTipo')->dropdownList($books, ['prompt'=>'Select Category']) ?>
        <?= $form->field($model, 'IdArtista') ?>
        <?= $form->field($model, 'Fecha_Inicio')->widget(DateControl::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>
        <?= $form->field($model, 'Fecha_Fin')->widget(DateControl::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>
        <?= $form->field($model, 'PrecioEntrada') ?>
    
        <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- searchadv -->
