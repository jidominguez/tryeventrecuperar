

<div class="item">
<div class="EventoImagen">
<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
if($model->Foto!=''){
echo "<img src=imagenes/$model->Foto class=Eimg>";
}else{
echo "<img src=imagenes/multimedia.png class=Eimg>";
}
?>

</div>
<div class="EventoNombre" > 
<h1>
<?php 
echo $model->Nombre;
?>
</h1>
</div>
<div class="EventoDireccion" > 
Dirección:
<?php 
echo $model->Direccion;
?>
</div>
<div class="EventoUsuario" >
Usuario:
<?php 
echo $model->usuario->Nombre;
?>
</div>
<div class="EventoTipo" > 
Tipo:
<?php 
echo $model->tipo->Descrip;
?>
</div>
<div class="EventoArtista"> 
Artista:
<?php 
echo $model->artista->Nombre;
?>
</div>
<?php
if(Yii::$app->user->isGuest){
    $form = ActiveForm::begin(['action'=>['/usuarios/create']]);
    echo "<div class=EventoCompra >"; 
    echo "<button id=iconocompra type=submit class=btn btn-success><strong>Necesitas registrarte</strong></button>";
    echo "</div>";
    ActiveForm::end();
}else{
if(Yii::$app->user->identity->IdRol==2 ){
    $form = ActiveForm::begin(['action'=>['/compraentradas/create']]);
    echo "<div class=EventoCompra >"; 
    echo "<input type=hidden name=idEvento value=$model->idEvento>";
    echo "<input type=hidden name=importe value=$model->PrecioEntrada>";
    echo "<input type=hidden name=VentaAnticipada value=$model->VentaAnticipada>";
    echo "<input type=number id=cuadro name=compra placeholder='$model->PrecioEntrada € por cada entrada'> <p>";
    echo "<button id=iconocompra type=submit class=btn btn-success><strong>Comprar</strong></button>";
    echo"</div>";
    ActiveForm::end();
    }else{
    echo "";
    }
}
?>
<div class="Recuadro">
<div class="EventoFechaInicio" > 
fecha de inicio:
<?php 
echo $model->FechaIni;
?>
</div>
<div class="EventoFechaFin" >
fecha de fin: 
<?php 
echo $model->FechaF;
?>
</div>
<div class="EventoDescripcion"> 
<?php 

use Codeception\Step\Action;
use app\models\Compraentradas;
echo $model->Descripcion;
?>
</div>
</div>
</div>


