<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use SebastianBergmann\Timer\Timer;
use kartik\datecontrol\DateControl;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */
/* @var $form yii\widgets\ActiveForm */
/*<?= $form->field($model, 'Foto')->fileInput()?>*/
$books = ArrayHelper::map(\app\models\Tiposeventos::find()->all(), 'IdTipo', 'Descrip');
?>

<div class="eventos-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Descripcion')->textarea(['rows' => '6']) ?>

    <?= $form->field($model, 'IdTipo')->dropdownList($books, ['prompt'=>'Select Category']) ?>

    <?= $form->field($model, 'IdArtista')->textInput() ?>
    
    <?= $form->field($model, 'imagefile')->fileInput(['maxlength' => true])?>

    <?= $form->field($model, 'Fecha_Inicio')->widget(DateControl::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>
       
    <?= $form->field($model, 'Hora_Inicio')->widget(TimePicker::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>

    <?= $form->field($model, 'Fecha_Fin')->widget(DateControl::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>    

    <?= $form->field($model, 'Hora_Fin')->widget(TimePicker::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>
    
    <?= $form->field($model, 'CantidadEntradas')->textInput() ?>

    <?= $form->field($model, 'PrecioEntrada')->textInput() ?>

    <?= $form->field($model, 'VentaAnticipada')->checkbox()?>

    <?= $form->field($model, 'Fecha_VentaAnticipada')->widget(DateControl::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>    

    <?= $form->field($model, 'Hora_VentaAnticipada')->widget(TimePicker::classname(),['pluginOptions' => ['autoclose'=>true]]) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
