<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\usuarios */

$this->title = 'Perfil del usuario '.$model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuarios-view">

    <h1><?= Html::encode($model->Nombre) ?></h1>

    <p>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Usuario',
            'Email:email',
            ['attribute'=>'Fecha de Nacimiento',
            'value'=>function($model,$attribute){
                return $model->fechaText;  
            }
           ],
            ['attribute'=>'IdRol',
             'value'=>function($model,$attribute){
                 return $model->RolText;  
             },
             'filter'=>app\models\Usuarios::$RolOptions,
            ],
        ],
    ]) ?>

</div>
