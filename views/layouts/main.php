<?php

/* @var $this \yii\web\View */
/* @var $content string */


use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

AppAsset::register($this);
//Con la ventana en un ordenador 100%

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="header">
    <h1 style="text-align:center;">
        <?php
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->IdRol=='3'){
        echo "<a href=index.php?r=eventos%2Fcreate>Crear un evento</a>";
        }else if(!Yii::$app->user->isGuest && Yii::$app->user->identity->IdRol=='2'){
        echo Yii::$app->user->identity->Punto." Puntos";
        } else {
        echo"Bienvenido a TryEvent.es";
        }
        ?>
    </h1>
</div>

<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'alt' => 'Registro de usuario',
        ],        
    ]);
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-center', 'id' => 'logo', 'alt'=>'Lista de eventos'],
        'encodeLabels'=>false,
        'items'=>[
            ['label'=>"<img src='imagenes/TryEventLogo2-removebg-preview.png' id='imagenlogo'>", 'url' => ['/eventos/index']],
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', ],
        'encodeLabels'=>false,
        'items' => [
            ['label' => "<img src='icons/buscar1.png'class='icono' alt='Busqueda avanzada'>", 'url'=>['/eventos/searchadv'], 'alt'=> 'buscador avanzado'],
                ['label' => "<img class='icono' src='icons/shop.png' alt='tienda de puntos'>", 'url'=>['/eventos/tiendapuntos'], 'alt' => 'Tienda de puntos'],
            Yii::$app->user->isGuest ? (
                
                ['label' => "<img src='icons/usuar.png'class='icono' alt='loguin'>", 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    "<img src='icons/salida.png' style='width:70px; margin-top:-15%'>",
                    ['class' => 'btn btn-link logout', 'alt'=>'Cerrar sesión']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer>
        <p class="pull-left">&copy; TryEvent <?= date('Y') ?></p>

</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
