<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compraentradas */
/* @var $form ActiveForm */
?>
<div class="compratiendapuntos">
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'VentaAnticipada')->hiddenInput(['value'=>$_POST['VentaAnticipada']])->label(false); ?>
    <?= $form->field($model, 'idEvento')->hiddenInput(['value'=>$_POST['idEvento']])->label(false); ?>
    <?= $form->field($model, 'CantidadEntradas')->hiddenInput(['value'=>$_POST['compra']])->label(false); ?>
    <?= $form->field($model, 'Puntos')->hiddenInput(['value'=>$_POST['compra']*10])->label(false); ?>
    <?= $form->field($model, 'Importe')->hiddenInput(['value'=>$_POST['importe']*$_POST['compra']])->label(false); ?>
    <?php
    echo "Serían un tontal de ".$_POST['compra']." Entradas por ".$_POST['importe']*$_POST['compra']."€";
    echo "¿Esta de acuerdo?";
    echo $_POST['VentaAnticipada'];
    ?>
    <div class="form-group">
        <?= Html::submitButton('Comprar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end();?>

</div><!-- compratiendapuntos -->
