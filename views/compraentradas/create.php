<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compraentradas */

$this->title = 'Create Compraentradas';
$this->params['breadcrumbs'][] = ['label' => 'Compraentradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compraentradas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
