<?php

use yii\helpers\Html;
use app\models\Usuarios;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compraentradas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compraentradas-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'VentaAnticipada')->hiddenInput(['value'=>$_POST['VentaAnticipada']])->label(false); ?>
    <?= $form->field($model, 'idEvento')->hiddenInput(['value'=>$_POST['idEvento']])->label(false); ?>
    <?= $form->field($model, 'CantidadEntradas')->hiddenInput(['value'=>$_POST['compra']])->label(false); ?>
    <?php
    if($_POST['VentaAnticipada']==0){
    ?>
        <?= $form->field($model, 'Puntos')->hiddenInput(['value'=>$_POST['compra']*10])->label(false); ?>
    <?php }else{ ?>
        <?= $form->field($model, 'Puntos')->hiddenInput(['value'=>$_POST['compra']*10-150])->label(false); ?>
    <?php } ?>
    <?= $form->field($model, 'Importe')->hiddenInput(['value'=>$_POST['importe']*$_POST['compra']])->label(false); ?>
    
    <?php

    echo "Serían un tontal de ".$_POST['compra']." Entradas por ".$_POST['importe']*$_POST['compra']."€";
    echo "¿Esta de acuerdo?";
    ?>
    <div class="form-group">
        <?= Html::submitButton('Comprar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
