<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompraentradasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compraentradas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idEvento') ?>

    <?= $form->field($model, 'idUsuario') ?>

    <?= $form->field($model, 'CantidadEntradas') ?>

    <?= $form->field($model, 'Puntos') ?>

    <?php // echo $form->field($model, 'Fecha_Compra') ?>

    <?php // echo $form->field($model, 'Importe') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
